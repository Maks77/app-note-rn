import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import moment from 'moment'


const NoteListItem = ({note, onPress}) => {

    const pressHandler = () => {
        onPress(note)
    }
    
    return (
        <TouchableOpacity onPress={pressHandler} activeOpacity={0.4}>
            <Container>
                <Content>
                    <NoteText numberOfLines={3}>{note.text}</NoteText>
                </Content>
                <View>
                    <DateText>{moment(note.created_at).format('DD.MM.YYYY')}</DateText>
                </View>
            </Container>
        </TouchableOpacity>
    );
}

const NoteText = styled.Text`
    font-size: 18px;
    color: #333;
    margin-right: 8px;
`

const DateText = styled.Text`
    font-size: 12px;
    color: #ccc;
`

const Content = styled.View`
    flex: 1;
`

const Container = styled.View`
    background: #fff;
    padding: 8px;
    padding-left: 16px;
    border-top-right-radius: 8px;
    border-bottom-right-radius: 8px;
    border-left-color: #F57270
    border-left-width: 4px;
    elevation: 4;
    margin: 8px;
    flex-direction: row;
`

export default NoteListItem;