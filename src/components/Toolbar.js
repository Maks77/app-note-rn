import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import styled from 'styled-components';

const Toolbar = () => {
    return (
        <View>
            <View style={StyleSheet.header}>
                <Text style={StyleSheet.headerText}></Text>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#333',
        letterSpacing: 1
    }
})

export default Toolbar;