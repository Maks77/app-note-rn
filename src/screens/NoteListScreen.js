import React, { useState, useLayoutEffect, useEffect} from 'react';
import { View, Text, TouchableOpacity, Button } from 'react-native';
import styled from 'styled-components';

import NoteListItem from '../components/NoteListItem'
import { Ionicons } from '@expo/vector-icons';

const NoteListScreen = ({ navigation, onAddPress }) => {

    const [notes, setNotesState] = useState([
        { id: 0, text: 'Some note of notes', created_at: Date.now() },
        { id: 1, text: 'Some anotherr note of notes', created_at: Date.now() }
    ])

    const addNote = (note) => {
        note.id = note.id.substring(0, note.id.length - 2)
        note.created_at = Date.now()
        setNotesState((prev) => {
            return [...prev, note]
        })
    }

    const updateNote = (note) => {
        let arr = [...notes]
        let elNote = arr.find(el => el.id === note.id)

        setNotesState((prev) => {
            return arr;
        })
    }

    const saveNoteHandler = (newNote) => {
        const is_create = newNote.id.toString().indexOf('_c') >= 0
        
        if (is_create) {
            addNote(newNote)
        } else {
            updateNote(newNote)
        }
        
    }

    const noteItemPressHandler = (note) => {
        navigation.navigate('NoteItemScreen', {
            note: note,
            onSave: saveNoteHandler,
            onUpdate: updateNote
        })
    }

    const addNotePressHandler = () => {
        const newNote = {
            id: Date.now().toString() + '_c',
            text: '',
            created_at: Date.now()
        }
        navigation.navigate('NoteItemScreen', {
            note: newNote,
            onSave: saveNoteHandler,
            onUpdate: updateNote
        })
    }

    useEffect(() => {
        console.log('NoteListScreen');
        return () => {
            console.log('NoteListScreen Do some cleanup');
        }
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity style={{marginRight: 16}} onPress={addNotePressHandler}>
                    <Ionicons name="md-add-circle-outline" size={36} color="green" />
                </TouchableOpacity>
            )
        })
    })

    return (
        <Container>
            {
                notes.map(note => <NoteListItem key={note.id} note={note} onPress={noteItemPressHandler}></NoteListItem>)
            }
        </Container>
    )
}

const Container = styled.View`
    padding-top: 8px;
`

export default NoteListScreen;