import React, {useState, useEffect, useLayoutEffect} from 'react';
import { View, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard, InteractionManager } from 'react-native';
import styled from 'styled-components';
import { Feather } from '@expo/vector-icons'; 

const NoteItemScreen = ({navigation, route}) => {
    const noteItem = route.params.note;

    const [text, setText] = useState(noteItem.text)
    let textAreaRef;
    
    
    const changeTextHandler = (text) => {
        setText(text)
    }

    const onSaveButtonClickHanlder = () => {
        if (text !== '') {
            noteItem.text = text
            route.params.onSave(noteItem)
            navigation.goBack();
        }
        
    }

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity style={{marginRight: 16}} onPress={onSaveButtonClickHanlder}>
                    <Feather name="save" size={36} color="#404447" />
                </TouchableOpacity>
            )
        })
    })

    return (
        <TouchableWithoutFeedback>
            <Container>
                <TextArea
                    autoCorrect={false}
                    onChangeText={text => changeTextHandler(text)}
                    multiline={true}
                    value={text}
                    textAlignVertical={'top'}
                    ref={(input) => { textAreaRef = input; }}
                ></TextArea>
            </Container>
        </TouchableWithoutFeedback>
    );
}

const TextArea = styled.TextInput`
    width: 100%;
    height: 100%;
    font-size: 18px;
    padding: 8px;
`
const Container = styled.View`
    flex: 1;
    justify-content: flex-start;
`

export default NoteItemScreen;